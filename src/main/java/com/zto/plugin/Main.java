package com.zto.plugin;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.util.List;

/**
 * Created by xingyuchao on 2017-05-31.
 * Description: 測試插件
 */
@Mojo(name = "zto-plugin",defaultPhase = LifecyclePhase.PACKAGE)
public class Main extends AbstractMojo {

    @Parameter
    private String msg;

    @Parameter
    private List<String> stringList;

    @Parameter(property = "args")
    private String args;

    public void execute() throws MojoExecutionException, MojoFailureException {

        System.out.println("this is test plugin...");

        System.out.println("this is test plugin...,msg:"+msg);
        System.out.println("this is test plugin...,stringList:"+stringList);
        System.out.println("this is test plugin...,args:"+args);
    }
}
